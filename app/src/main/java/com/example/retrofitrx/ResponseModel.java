package com.example.retrofitrx;

public class ResponseModel {
    private String name;

    public ResponseModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
