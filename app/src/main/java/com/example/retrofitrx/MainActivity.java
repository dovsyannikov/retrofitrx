package com.example.retrofitrx;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    Disposable disposable;
    


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        disposable = ApiService.getData()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer<List<ResponseModel>>() {
//                    @Override
//                    public void accept(List<ResponseModel> responseModels) throws Exception {
//                        Toast.makeText(MainActivity.this, responseModels.get(0).getName(), Toast.LENGTH_SHORT).show();
//                    }
////                }, new Consumer<Throwable>() {
////                    @Override
////                    public void accept(Throwable throwable) throws Exception {
////                        Toast.makeText(MainActivity.this, "Smth went wrong", Toast.LENGTH_SHORT).show();
////                    }
//                });

        disposable = ApiService.getCountryByCapital("Kiev")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<ResponseModel>>() {
                    @Override
                    public void accept(List<ResponseModel> responseModels) throws Exception {
                        Toast.makeText(MainActivity.this, responseModels.get(0).getName(), Toast.LENGTH_SHORT).show();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "Smth went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
