package com.example.retrofitrx;

import com.example.retrofitrx.ResponseModel;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://restcountries.eu/rest/v2/";
    private static PrivateApi privateApi;

    public interface PrivateApi {
        @GET("all")
        Observable<List<ResponseModel>> getCountries();

        @GET("capital/{capital}")
        Observable<List<ResponseModel>> getCountry(@Path("capital") String capital);
    }


    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Observable<List<ResponseModel>> getData(){
        return privateApi.getCountries();
    }

    public static Observable<List<ResponseModel>> getCountryByCapital (String capital){
        return privateApi.getCountry(capital);
    }
}
